#include "systemintegration.h"

#ifdef Q_OS_WIN // for wallpaper set on windows platform
#include <windows.h>
#endif

#if QT_VERSION < 0x050000
#include <QDesktopServices>
#else
#include <QStandardPaths>
#endif

#include <QProcessEnvironment>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QSettings>

#include <QDebug>

QVector<SystemIntegration::register_base*> SystemIntegration::m_list;
int SystemIntegration::currentDesktopEnvironment = -1;

template<class C>
const int SystemIntegration::register_<C>::ID = SystemIntegration::Register(new SystemIntegration::register_<C>());

QStringList saveWallpapers(QVector<QImage> images,
                                             const char* format)
{
    QStringList paths;

//    QString pathTemplate("/home/mhn/image%1.png");
#if QT_VERSION < 0x050000
    QDir dir(QDesktopServices::storageLocation(QDesktopServices::CacheLocation));
#else
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
#endif
//    qDebug() << dir.filePath("image%1.png");
    dir.mkpath(".");
    QString pathTemplate(dir.filePath("wallpaper%2.%1"));

    pathTemplate = pathTemplate.arg(format);

    for(int i = 0; i <images.size(); i++)
    {
        QString path = pathTemplate.arg(i);
//        qDebug() << "Saving image for screen" << i << path;
        if (!images.at(i).save(path, format))
            qDebug() << "Failed to save image!";
        paths << path;
    }
    return paths;
}

#ifdef Q_OS_WIN

class Windows : public DesktopEnvironment, private SystemIntegration::register_<Windows> {
public:
    static const int factory_key = 0;
    Windows() {}

    static bool isCurrent()
    {
        return true;
    }

    static QString name()
    {
        return "windows";
    }

    static SystemIntegration::NumberOfWallpapers mode()
    {
        return SystemIntegration::JustOne;
    }

    static SystemIntegration::ReturnValue applyWallpapers(QVector<QImage> images)
    {
        QStringList paths = saveWallpapers(images, "bmp");

        if (paths.size() != 1)
        {
            qDebug() << "wrong number of images:" << paths.size() << "(expected 1)";
            return SystemIntegration::ReadError;
        }

        QSettings appSettings( "HKEY_CURRENT_USER\\Control Panel\\Desktop",
                               QSettings::NativeFormat);
        appSettings.setValue("Wallpaper", paths.at(0) );
        appSettings.setValue("WallpaperStyle", "0");
        appSettings.setValue("TileWallpaper", "1");
        QByteArray ba = paths.at(0).toLatin1();
        SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0,
                              reinterpret_cast<void*>( ba.data() ),
                              SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
//        qDebug() << name() << "setting wallpaper to" << images;
        return SystemIntegration::Success;
    }
};

#else // Q_OS_WIN

class Kde_Plasma : public DesktopEnvironment, private SystemIntegration::register_<Kde_Plasma> {
public:
    static const int factory_key = 0;
    Kde_Plasma() {}

    static bool isCurrent()
    {
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        return env.value("DESKTOP_SESSION", "unknown") == "kde-plasma" ||
                (env.value("KDE_FULL_SESSION", "false") == "true" &&
                 env.value("KDE_SESSION_VERSION", "unknown") == "4");
    }

    static QString name()
    {
        return "KDE 4 (Plasma)";
    }

    static SystemIntegration::NumberOfWallpapers mode()
    {
        return SystemIntegration::OnePerScreen;
    }

    static SystemIntegration::ReturnValue applyWallpapers(QVector<QImage> images)
    {
        QStringList paths = saveWallpapers(images, "png");

        for (int i = 0; i < paths.size(); i++)
        {
            QString path = paths.at(i);

            QFile file(path);
            QFileInfo fileinfo(file);

            QString newPath = QString("%1/%2_kde.%3").arg(fileinfo.path()).arg(fileinfo.baseName()).arg(fileinfo.suffix());

            QFile newFile(newPath);

            if (newFile.exists())
                newFile.remove();

            if (!file.rename(newPath))
            {
                qDebug() << "Failed to rename file" << path << newPath;
                return SystemIntegration::WriteError;
            }
        }
        return SystemIntegration::Success;
    }
};

#endif // Q_OS_WIN
