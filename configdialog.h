#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include "types.h"

#include <QDialog>
#include <QStringListModel>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QSignalMapper>
#include <QGraphicsScene>

#include "screenview.h"
#include "screenrectconfig.h"

class QAbstractButton;

namespace Ui {
class ConfigDialog;
}

class ConfigDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ConfigDialog(QWidget *parent = 0);
    ~ConfigDialog();

    void setDirectories(QStringList paths);
    QStringList getDirectories();

    void setInterval(unsigned int interval);
    unsigned int getInterval();

    void setRecursive(bool recursive);
    bool getRecursive();

    void setScreenConfig(WTypes::ScreenList screenConfig, WTypes::HostList hostConfig);
    void getScreenConfig(WTypes::ScreenList *screenConfig, WTypes::HostList *hostConfig);

    void setColor(QColor color);
    QColor getColor();

    void setClientConfig(QString clientHost, quint16 clientPort);
    void getClientConfig(QString *clientHost, quint16 *clientPort);
    void setServerConfig(quint16 serverPort);
    quint16 getServerConfig();

public slots:
    void addDirectory();
    void removeDirectory();
    void chooseColor();
    void onWallpapersChanged(QVector<WTypes::Wallpaper> wallpapers);
    void onWallpapersChanging();
    void deleteWallpaper(int index);

signals:
    void quit();
    void openConfig();
    void apply();
    void deleteWallpaper(QString path);
    void changeWallpaper();
    
private:
    Ui::ConfigDialog *ui;
    QSystemTrayIcon trayicon;
    QMenu traymenu;
    QMenu *subMenu;
    QIcon normalIcon, busyIcon;
    QSignalMapper deleteMapper;
    QStringListModel model;
    QGraphicsScene screenScene;
    QString lastPath;
    QVector<ScreenRectConfig*> screenRectConfigs;
    WTypes::ScreenList temp_screenConfig;
    WTypes::HostList temp_hostConfig;
    QVector<WTypes::Wallpaper> cur_wallpapers;
};

#endif // CONFIGDIALOG_H
