#include "screenview.h"

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QDebug>

ScreenView::ScreenView(QWidget *parent) :
    QGraphicsView(parent)
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void ScreenView::mouseMoveEvent(QMouseEvent *event)
{
    QGraphicsView::mouseMoveEvent(event);
    refitScene();
}

void ScreenView::resizeEvent(QResizeEvent *event)
{
    QGraphicsView::resizeEvent(event);
    refitScene();
}

void ScreenView::refitScene()
{
    this->fitInView(this->scene()->itemsBoundingRect(), Qt::KeepAspectRatio);
}
