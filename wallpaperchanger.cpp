#include "wallpaperchanger.h"

#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <QtSvg/QSvgRenderer>
#include <QPainter>

WallpaperChanger::WallpaperChanger(const ImageRepository *repository,
                                   const WallpaperServer *remoteWC,
                                   QObject *parent) :
    QObject(parent),
    repo(repository),
    remote(remoteWC)
{
}

QVector<WTypes::Wallpaper> WallpaperChanger::getNewWallpapers() const
{
    QVector<WTypes::Wallpaper> newWallpapers;

    QVector<bool> changed(m_screenConfigurations.size(), false);

    QStringList exclude(excludePaths);

    WTypes::ScreenSizeList curSizes(m_screenSizes);

    do {
        QList<QSize> sizes;

        foreach (const WTypes::ScreenSize &screenSize, curSizes)
            sizes << screenSize.size;

        WTypes::Image img;

        if (!repo->getImage(sizes, exclude, &img))
        {
            qDebug() << "No suitable wallpapers found for sizes:" << sizes;
            break;
        }


        exclude << img.path;

        QVector<int> affectedScreens;

        for (int i = 0; i < curSizes.size(); i++)
            if (curSizes[i].size == img.imageSize)
            {
                affectedScreens = curSizes[i].screens;
                break;
            }

        foreach(int screen, affectedScreens)
        {
            for (int i = curSizes.size() - 1; i >= 0;  i--)
            {
                if (curSizes.at(i).screens.contains(screen))
                {
                    curSizes.remove(i);
                }
            }
            changed[screen] = true;
        }

        WTypes::Wallpaper newWallpaper;
        newWallpaper.screens = affectedScreens;
        newWallpaper.image = img;

        newWallpapers.append(newWallpaper);
    } while (changed.contains(false));

    return newWallpapers;
}

void WallpaperChanger::svgWorkAround(const QString path, QImage *image) const
{
    QFileInfo fileinfo(path);
    if (fileinfo.completeSuffix().toLower() == "svg" ||
            fileinfo.completeSuffix().toLower() == "svgz")
    {
        QSvgRenderer renderer;
        if (!renderer.load(path))
            return;

        image->fill(backGroundcolor);
        QPainter painter(image);

        renderer.render(&painter);
    }
}

void WallpaperChanger::createExcludeFilter(const QVector<WTypes::Wallpaper> &wallpapers)
{
    excludePaths.clear();
    foreach (const WTypes::Wallpaper &wp, wallpapers) {
        excludePaths << wp.image.path;
    }
}

QVector<QImage> WallpaperChanger::createNewWallpapers(
        QVector<WTypes::Wallpaper> &newWallpapers) const
{
    QVector<QImage> images(m_screenConfigurations.size());

    for (int wallpaper = 0; wallpaper < newWallpapers.size(); wallpaper++)
//    foreach(Wallpaper &newWallpaper, newWallpapers)
    {
        WTypes::Wallpaper &newWallpaper = newWallpapers[wallpaper];
//        qDebug() << "Gonna put" << newWallpaper.image.path << "on screens" << newWallpaper.screens;

        QImage image(newWallpaper.image.path);

        newWallpaper.thumbnail = image.scaled(WTypes::thumbNailSize, WTypes::thumbNailSize, Qt::KeepAspectRatio,
                                              Qt::SmoothTransformation);

        image = image.scaled(newWallpaper.image.imageSize,
                             Qt::KeepAspectRatioByExpanding,
                             Qt::SmoothTransformation);

        svgWorkAround(newWallpaper.image.path, &image);

        QRect rect;
        foreach(int screen, newWallpaper.screens)
        {
            rect = rect.united(m_screenConfigurations[screen].tweaked);
        }
//        qDebug() << rect.topLeft() << image.size();

        QSize diffs ((image.size() - rect.size()) / 2);
        QPoint diff(diffs.width(), diffs.height());
        diff -= rect.topLeft();

        foreach(int screen, newWallpaper.screens)
        {
            QRect clipRect = m_screenConfigurations[screen].tweaked
                    .translated(diff);
//            qDebug() << "CLIPRECT" << clipRect;
            images[screen] = image.copy(clipRect);
        }
    }
    return images;
}

QVector<QVector<QImage> > WallpaperChanger::prepareWallpapersForHosts(
        const QVector<QImage> images) const
{
    QVector<QVector<QImage> > hostImages;

    for (int host = 0; host < hosts.size(); host++)
    {
        QVector<QImage> newImages;
        if (hosts.at(host).mode == WTypes::JustOne)
        {
            QRect rect;
            foreach (int screen, hosts.at(host).screens)
            {
                rect = rect.united(m_screenConfigurations[screen].actual);
            }

            QImage image(rect.size(), QImage::Format_ARGB32);

            image.fill(backGroundcolor);

            QPainter painter(&image);

            foreach (int screen, hosts.at(host).screens)
            {
                painter.drawImage(m_screenConfigurations[screen].actual,
                                  images[screen]);
            }
            newImages << image;
        }
        else if (hosts.at(host).mode == WTypes::OnePerScreen)
        {
            foreach (int screen, hosts.at(host).screens)
            {
                QImage image(images.at(screen).size(), QImage::Format_ARGB32);
                image.fill(backGroundcolor);
                QPainter painter(&image);
                painter.drawImage(0, 0, images[screen]);
                newImages << image;
            }
        }
        hostImages.append(newImages);
    }
    return hostImages;
}

void WallpaperChanger::changeWallpaper()
{
    if (hosts.empty())
        return;

    remote->notifyClients("changing");
//    qDebug() << "CHANGING WP";
    QVector<WTypes::Wallpaper> newWallpapers = getNewWallpapers();

//    foreach (const WTypes::Wallpaper &wp, newWallpapers)
//    {
//        qDebug() << "WP" << wp.image.path << wp.screens;
//    }

    QVector<QImage> images = createNewWallpapers(newWallpapers);

//    foreach (const QImage &img, images)
//    {
//        qDebug() << "IMG" << img.size();
//    }

    QVector<QVector<QImage> > hostImages = prepareWallpapersForHosts(images);

//    foreach (const QVector<QImage> &imgs, hostImages)
//    {
//        qDebug() << "IMGS";
//        foreach (const QImage &img, imgs)
//        {
//            qDebug() << "IMG" << img.size();
//        }
//    }

//    foreach (const WTypes::HostConfig &hostconf, hosts)
//    {
//        qDebug() << "HOST" << hostconf.screens;
//    }


    remote->applyWallpapers(hostImages, newWallpapers);

    createExcludeFilter(newWallpapers);

//    emit wallpaperChanged(newWallpapers);
}

void WallpaperChanger::changeScreenConfig(WTypes::ScreenList newConfig,
                                          WTypes::HostList hostConfig)
{
//    qDebug() << "chanin";
    hosts = hostConfig;
    m_screenSizes.clear();

    for (int counter = 0; counter < (1 << newConfig.size()); counter++)
    {
        QVector<int> included, excluded;
        QRect includedRect, excludedRect;

        for (int i = 0; i < newConfig.size(); ++i)
        {
            if (counter & (1 << i))
            {
                included.append(i);
                includedRect = includedRect.united(newConfig[i].tweaked);
            }
            else
            {
                excluded.append(i);
                excludedRect = excludedRect.united(newConfig[i].tweaked);
            }
        }
        bool include(false);
        if (included.size() > 0)
        {
            if (!includedRect.intersects(excludedRect) || excluded.size() != 1)
            {
                bool intersects(false);
                foreach (int i, excluded)
                {
                    if (includedRect.intersects(newConfig[i].tweaked))
                    {
                        intersects = true;
                        break;
                    }
                }
                include = !intersects;
            }
        }

        if (include)
        {
//                    qDebug() << "included" << includedRect << "excluded" << excludedRect;
            WTypes::ScreenSize sc;
            sc.screens = included;
            sc.size = includedRect.size();
            m_screenSizes.append(sc);
        }
    }
    for (int i = 0; i < m_screenSizes.size(); i++)
    {
        for (int j = i; j < m_screenSizes.size(); j++)
        {
            if (m_screenSizes[j].size.width() * m_screenSizes[j].size.height()
                    > m_screenSizes[i].size.width() *
                    m_screenSizes[i].size.height())
            {
                WTypes::ScreenSize sc = m_screenSizes[i];
                m_screenSizes[i] = m_screenSizes[j];
                m_screenSizes[j] = sc;
            }
        }
    }
    foreach (const WTypes::ScreenSize &screenconf, m_screenSizes)
    {
        qDebug() << "Screens:" << screenconf.screens << "Size:" << screenconf.size;
    }
    m_screenConfigurations = newConfig;
}


void WallpaperChanger::changeBackgroundColor(QColor color)
{
    backGroundcolor = color;
}
