#-------------------------------------------------
#
# Project created by QtCreator 2013-01-01T13:35:48
#
#-------------------------------------------------

QT       += core gui svg network

TARGET = walle
TEMPLATE = app


SOURCES += main.cpp \
    walle.cpp \
    systemintegration.cpp \
    networkserver.cpp \
    imagerepository.cpp \
    wallpaperchanger.cpp \
    networkclient.cpp \
    configdialog.cpp \
    screenview.cpp \
    screengraphicsitem.cpp \
    screenrectconfig.cpp

HEADERS  += \
    walle.h \
    types.h \
    systemintegration.h \
    networkserver.h \
    imagerepository.h \
    wallpaperchanger.h \
    networkclient.h \
    configdialog.h \
    screenview.h \
    screengraphicsitem.h \
    screenrectconfig.h

FORMS += \
    configdialog.ui

RESOURCES += \
    resources.qrc
