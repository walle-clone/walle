#include <QApplication>

#include "walle.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Walle w(&a);
    
    return a.exec();
}
