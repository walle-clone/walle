#ifndef SCREENRECTCONFIG_H
#define SCREENRECTCONFIG_H

#include <QWidget>
#include <QSpinBox>
#include <QRect>

class ScreenRectConfig : public QWidget
{
    Q_OBJECT
public:
    explicit ScreenRectConfig(const QRect &rect, QWidget *parent = 0);

    QRect getScreenRect();
signals:
    void valueChanged(QRect rect);
    
public slots:
    void onValueChanged();
    void setValue(QRect rect);

private:
    QSpinBox xpos, ypos, height, width;
    
};

#endif // SCREENRECTCONFIG_H
