#include "screenrectconfig.h"

#include <QLabel>
#include <QLayout>
#include <QHBoxLayout>

ScreenRectConfig::ScreenRectConfig(const QRect &rect, QWidget *parent) :
    QWidget(parent)
{
    xpos.setMaximum(10000);
    xpos.setMinimum(-10000);
    ypos.setMaximum(10000);
    ypos.setMinimum(-10000);

    height.setMaximum(10000);
    width.setMaximum(10000);

    setLayout(new QHBoxLayout());
    layout()->addWidget(new QLabel("Position: "));
    layout()->addWidget(&xpos);
    layout()->addWidget(&ypos);
    layout()->addWidget(new QLabel("Size: "));
    layout()->addWidget(&width);
    layout()->addWidget(&height);

    connect(&xpos, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged()));
    connect(&ypos, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged()));
    connect(&width, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged()));
    connect(&height, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged()));

    setValue(rect);
}

QRect ScreenRectConfig::getScreenRect()
{
    return QRect(xpos.value(), ypos.value(), width.value(), height.value());
}

void ScreenRectConfig::onValueChanged()
{
    emit valueChanged(getScreenRect());
}

void ScreenRectConfig::setValue(QRect rect)
{
    xpos.setValue(rect.x());
    ypos.setValue(rect.y());
    height.setValue(rect.height());
    width.setValue(rect.width());
}
