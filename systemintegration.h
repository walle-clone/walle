#ifndef SYSTEMINTEGRATION_H
#define SYSTEMINTEGRATION_H

#include <QString>
#include <QImage>
#include <QVector>

class ImageHandler;

class DesktopEnvironment {
public:
    virtual ~DesktopEnvironment()
    {}
};

class SystemIntegration {
public:

    enum ReturnValue {
        Success,
        UnsupportedDesktopEnvironment,
        ReadError,
        WriteError,
        UnknownDesktopEnvironment,
        NoScreensDetected,
        SkipImage
    };

    enum NumberOfWallpapers {
        OnePerScreen,
        JustOne,
        NoWallpaper
    };

    struct register_base
    {
        virtual int id() const = 0;
        virtual bool isCurrent_() = 0;
        virtual QString getName_() = 0;
        virtual NumberOfWallpapers getMode_() = 0;
        virtual ReturnValue applyWallpapers_(QVector<QImage> images) = 0;
    };

    template<class C>
    struct register_ : register_base
    {
        static const int ID;
        register_() :
            id_(ID)
        {}
        int id() const
        {
            return C::factory_key;
        }
        bool isCurrent_()
        {
            return C::isCurrent();
        }
        QString getName_()
        {
            return C::name();
        }
        NumberOfWallpapers getMode_()
        {
            return C::mode();
        }
        ReturnValue applyWallpapers_(QVector<QImage> images)
        {
            return C::applyWallpapers(images);
        }

    private:
        const int id_;
    };

    static int Register(register_base* t)
    {
        m_list.push_back(t);
        return m_list.size();
    }

    static NumberOfWallpapers getMode()
    {
        getCurrent();
        if (currentDesktopEnvironment < 0)
            return NoWallpaper;
        return m_list.at(currentDesktopEnvironment)->getMode_();
    }

    static QString getName()
    {
        getCurrent();
        if (currentDesktopEnvironment < 0)
            return "Unknown";
        return m_list.at(currentDesktopEnvironment)->getName_();
    }

    static ReturnValue applyWallpapers(QVector<QImage> images)
    {
        getCurrent();
        if (currentDesktopEnvironment < 0)
            return UnknownDesktopEnvironment;
        return m_list.at(currentDesktopEnvironment)->applyWallpapers_(images);
    }


    static size_t getPluginCount()
    {
        return m_list.size();
    }

private:
    static void getCurrent()
    {
        if (currentDesktopEnvironment < 0)
            for (int i = 0; i < m_list.size(); i++)
                if (m_list.at(i)->isCurrent_())
                    currentDesktopEnvironment = i;
    }

    static QVector<register_base*> m_list;
    static int currentDesktopEnvironment;
};


#endif // SYSTEMINTEGRATION_H
