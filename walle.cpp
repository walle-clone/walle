#include "walle.h"

#include <QApplication>
#include <QDebug>
#include <QSettings>

#include <QDesktopWidget>

Walle::Walle(QApplication *a) :
    QObject(),
    app(a),
    wpchanger(&imageRepository, &server)
{
    QCoreApplication::setOrganizationName("Mhn");
    QCoreApplication::setApplicationName("Walle");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    client.moveToThread(&clientThread);
    imageRepository.moveToThread(&serverThread);
    server.moveToThread(&serverThread);
    wpchanger.moveToThread(&serverThread);

    QDesktopWidget widget;

    QVector<QRect> screens;
    for (int s = 0; s < widget.screenCount(); s++)
        screens << widget.screenGeometry(s);
    client.setScreens(screens);

    connect(&dialog, SIGNAL(accepted()),
            this, SLOT(applySettings()));
    connect(&dialog, SIGNAL(apply()),
            this, SLOT(applySettings()));
    connect(&dialog, SIGNAL(quit()),
            this, SLOT(quit()));
    connect(&dialog, SIGNAL(openConfig()),
            this, SLOT(openSettings()));
//    connect(&dialog, SIGNAL(deleteCurrentWallpaper()),
//            this, SLOT(deleteWallpaper()));
    connect(&dialog, SIGNAL(changeWallpaper()),
            this, SIGNAL(requestWallpaperChanged()));
    qRegisterMetaType<QVector<WTypes::Wallpaper> >("QVector<WTypes::Wallpaper>");
    connect(&client, SIGNAL(wallpapersChanged(QVector<WTypes::Wallpaper>)),
            &dialog, SLOT(onWallpapersChanged(QVector<WTypes::Wallpaper>)), Qt::QueuedConnection);
    connect(&client, SIGNAL(changingWallpapers()),
            &dialog, SLOT(onWallpapersChanging()), Qt::QueuedConnection);
    connect(&dialog, SIGNAL(deleteWallpaper(QString)),
            &client, SLOT(deleteWallpaper(QString)), Qt::QueuedConnection);
//    qRegisterMetaType<WTypes::HostList>("WTypes::HostList");
//    qRegisterMetaType<WTypes::ScreenList>("WTypes::ScreenList");
    connect(this, SIGNAL(changeWallpaper()),
            &wpchanger, SLOT(changeWallpaper()), Qt::QueuedConnection);

//    qRegisterMetaType<QVector<WTypes::Wallpaper> >("QVector<WTypes::Wallpaper>");
//    connect(&wpchanger, SIGNAL(wallpaperChanged(QVector<WTypes::Wallpaper>)),
//            this, SLOT(wallpaperChanged(QVector<WTypes::Wallpaper>)), Qt::QueuedConnection);
    connect(this, SIGNAL(requestWallpaperChanged()),
            &client, SLOT(changeWallpaper()), Qt::QueuedConnection);
    connect(this, SIGNAL(connectToHost(QString,quint16)),
            &client, SLOT(connectToHost(QString,quint16)), Qt::QueuedConnection);

    connect(&server, SIGNAL(changeWallpaper()), this, SLOT(explicitlyChangeWallpaper()), Qt::QueuedConnection);

    connect(this, SIGNAL(pathsChanged(QStringList,bool)),
            &imageRepository, SLOT(setDirectories(QStringList,bool)), Qt::QueuedConnection);

    qRegisterMetaType<WTypes::HostList>("WTypes::HostList");
    qRegisterMetaType<WTypes::ScreenList>("WTypes::ScreenList");
    connect(this, SIGNAL(changeScreenConfig(WTypes::ScreenList,WTypes::HostList)),
            &wpchanger, SLOT(changeScreenConfig(WTypes::ScreenList,WTypes::HostList)), Qt::QueuedConnection);
    connect(this, SIGNAL(backgroundColorChanged(QColor)),
            &wpchanger, SLOT(changeBackgroundColor(QColor)), Qt::QueuedConnection);

    connect(this, SIGNAL(listenOnPort(quint16)), &server, SLOT(listenOnPort(quint16)));
    connect(&server, SIGNAL(listeningOnPort(quint16)), this, SLOT(readSettingsAndApply(quint16)));
    connect(&server, SIGNAL(hostConfigChanged(WTypes::ScreenList,WTypes::HostList)),
            this, SLOT(hostConfigChanged(WTypes::ScreenList,WTypes::HostList)));

    serverThread.start();
    clientThread.start();

    timer.setSingleShot(false);
    connect(&timer, SIGNAL(timeout()), &wpchanger, SLOT(changeWallpaper()));

//    timer.setSingleShot(false);
//    connect(&timer, SIGNAL(timeout()),
//            wpchanger, SLOT(changeWallpaper()));

//    qDebug() << "int" << interval;
//    timer.start(interval * 1000);

    readSettingsAndApply();

//    QTimer::singleShot(1000, this, SLOT(changeToNextWallpaper()));

//    readSettingsAndApply();
}

void Walle::openSettings()
{
    dialog.setAttribute(Qt::WA_QuitOnClose, false);

    readSettingsAndApply();
    dialog.show();
}

void Walle::applySettings()
{
    getAndWriteSettings();
    readSettingsAndApply();
}

void Walle::quit()
{
    qDebug() << "quitting ";

//    getAndWriteSettings();

    clientThread.quit();
    serverThread.quit();

    app->quit();
}

//void Walle::changeToNextWallpaper()
//{
//    if (files.size() == 0 || (files.size() == 1 && currentWallpaperIndex >= 0))
//    {
//        //        currentWallpaper.clear();
//    }
//    else
//    {
//        int random = (int)qrand() % (files.size() - (currentWallpaperIndex < 0 ? 0 : 1));

////        if (random >= currentWallpaperIndex && currentWallpaperIndex >= 0) random++;

////        int newWallpaperIndex = random;

//        QString newWallpaper;

//        QHashIterator<QString, QString> i(files);
//        while (i.hasNext()) {
//            i.next();

//            if (random == 0)
//            {
//                newWallpaper = i.value();
//                break;
//            }
////            else
////            {
////                random -= i.value().size();
//                random--;
////            }
//        }

//        if (!newWallpaper.isEmpty())
//        {
////            qDebug() << "got new wp!" << newWallpaper;

////            emit setWallpaper(newWallpaper, newWallpaperIndex);
//        }

////        currentWallpaper = newWallpaper;
////        currentWallpaperIndex = newWallpaperIndex;
//    }
//}

void Walle::explicitlyChangeWallpaper()
{
    timer.start();
    emit changeWallpaper();
//    changeToNextWallpaper();
}

void Walle::deleteWallpaper()
{
//    QMessageBox msgBox;
//    msgBox.setAttribute(Qt::WA_QuitOnClose, false);

////    msgBox.setText(tr("This will delete %1 permanently.").arg(currentWallpaper));
//    msgBox.setInformativeText("Are you sure?");
//    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
//    msgBox.setDefaultButton(QMessageBox::Cancel);
//    int ret = msgBox.exec();

//    if (ret == QMessageBox::Ok)
//    {
////        qDebug() << "Deleting" << currentWallpaper;
////        QFile file(currentWallpaper);
////        if (!file.remove())
////            qDebug() << file.errorString();
////        else
////            nextWallpaper();
//    }
}

void Walle::wallpaperChanged(QVector<WTypes::Wallpaper> wallpapers)
{
    foreach (const WTypes::Wallpaper &wallpaper, wallpapers)
    {
        qDebug() << "Wallpaper has been changed to" << wallpaper.image.path << "on screens" << wallpaper.screens;
    }
}

void Walle::hostConfigChanged(WTypes::ScreenList screens,
                              WTypes::HostList hosts)
{
    writeScreenConfig(screens, hosts, false);
//    QStringList hostnames;
//    for (int host = 0; host < hosts.size(); host++)
//    {
//        hostnames << hosts.at(host).hostname;
//    }
//    currentHostConfig = hostnames;

//    if (!hosts.empty())
//    {
//        QSettings settings;
//        settings.beginGroup(QString("hostconfiguration-%1").arg(currentHostConfig.join("_")));
//        {
//            settings.beginWriteArray("screen");
//            {
//                for (int screen = 0; screen < screens.size(); screen++)
//                {
//                    settings.setArrayIndex(screen);
//                    settings.setValue("actual", screens.at(screen).actual);
//                    //                settings.setValue("tweaked", screens.at(screen).tweaked);
//                }
//            }
//            settings.endArray();
//            settings.beginWriteArray("host");
//            {
//                for (int host = 0; host < hosts.size(); host++)
//                {
//                    settings.setArrayIndex(host);
//                    QStringList screens;
//                    foreach (int screen, hosts.at(host).screens)
//                        screens << QString("%1").arg(screen);
//                    settings.setValue("screens", screens);
//                    settings.setValue("mode", hosts.at(host).mode);
//                    settings.setValue("mode", hosts.at(host).hotsname);
//                }
//            }
//            settings.endArray();
//        }
//        settings.endGroup();
//    }
    readSettingsAndApply();
//    explicitlyChangeWallpaper();
}

void Walle::writeScreenConfig(WTypes::ScreenList screens,
                              WTypes::HostList hosts, bool writeTweaked)
{
    QStringList hostnames;
    for (int host = 0; host < hosts.size(); host++)
    {
        hostnames << hosts.at(host).hostname;
    }
    currentHostConfig = hostnames;

    if (!hosts.empty())
    {
        QSettings settings;
        settings.beginGroup(QString("hostconfiguration-%1").arg(currentHostConfig.join("_")));
        {
            settings.beginWriteArray("screen");
            {
                for (int screen = 0; screen < screens.size(); screen++)
                {
                    settings.setArrayIndex(screen);
                    settings.setValue("actual", screens.at(screen).actual);
                    if (writeTweaked)
                        settings.setValue("tweaked", screens.at(screen).tweaked);
                }
            }
            settings.endArray();
            settings.beginGroup("host");
            {
                for (int host = 0; host < hosts.size(); host++)
                {
                    settings.beginGroup(hosts.at(host).hostname);
//                    settings.setArrayIndex(host);
                    QStringList screens;
                    foreach (int screen, hosts.at(host).screens)
                        screens << QString("%1").arg(screen);
                    settings.setValue("screens", screens);
                    settings.setValue("mode", hosts.at(host).mode);
//                    settings.setValue("hostname", hosts.at(host).hostname);
                    settings.endGroup();
                }
            }
            settings.endGroup();
        }
        settings.endGroup();
    }
}

//void Walle::serverListening(quint16 port)
//{
//    if ()
//}


//void Walle::skipImage(QString wallpaper, int index)
//{
//    QMutableHashIterator<QString, QString> i(files);

//    while (i.hasNext()) {
//        i.next();

//        if (index == 0)
//        {
//            i.remove();
//            break;
//        }
//        index--;
//    }
//    qDebug() << "Skipping wallpaper:" << wallpaper << "files left:" << files.size();
////    changeToNextWallpaper();
//}


void Walle::readSettingsAndApply(quint16 explicitClientPort)
{
    QSettings settings;

//    qDebug() << settings.fileName();

    settings.beginGroup("settings");
    QStringList paths(settings.value("paths").toStringList());
    bool recursive = settings.value("recursive", false).toBool();
    settings.endGroup();
    settings.beginGroup("server");
    int interval = settings.value("interval", 180).toInt();
    quint16 serverPort = settings.value("port", 0).toUInt();
    settings.endGroup();
    settings.beginGroup("client");
    QString clientHost = settings.value("host", "localhost").toString();
    quint16 clientPort = settings.value("port", 0).toUInt();
    settings.endGroup();
    settings.beginGroup("wallpaper");
    QColor color(settings.value("color", QColor(Qt::white)).toString());
    settings.endGroup();

    if (clientPort == 0)
        clientPort = explicitClientPort;

    if (clientHost.isEmpty())
        clientHost = "localhost";

    WTypes::ScreenList screens;
    WTypes::HostList hosts;

    settings.beginGroup(QString("hostconfiguration-%1").arg(currentHostConfig.join("_")));
//    settings.beginGroup("screens");
    int size = settings.beginReadArray("screen");
    for (int s = 0; s < size; s++)
    {
        settings.setArrayIndex(s);
        QRect actual = settings.value("actual").toRect();
        QRect tweaked = settings.value("tweaked").toRect();

        if (!tweaked.isValid())
            tweaked = actual;

        WTypes::ScreenConfig screen(actual, tweaked);
        screens << screen;
    }
    settings.endArray();
    settings.beginGroup("host");
    for (int h = 0; h < currentHostConfig.size(); h++)
    {
        settings.beginGroup(currentHostConfig.at(h));

        WTypes::HostConfig host;
        host.mode = WTypes::NumberOfWallpapers(settings.value("mode").toUInt());
        QStringList screenstrings = settings.value("screens").toStringList();
        host.hostname = currentHostConfig.at(h);
        for (int s = 0; s < screenstrings.size(); s++)
            host.screens << screenstrings.at(s).toInt();
        hosts << host;
        settings.endGroup();
    }
    settings.endGroup();
    settings.endGroup();


    if (timer.interval() != interval*1000)
    {
        timer.setInterval(interval*1000);
        timer.start();
    }

    emit listenOnPort(serverPort);
    emit pathsChanged(paths, recursive);
    emit backgroundColorChanged(color);
    emit changeScreenConfig(screens, hosts);
    if (clientPort != 0)
        emit connectToHost(clientHost, clientPort);
    qDebug() << "Read settings and apply" << clientHost << clientPort;

    dialog.setColor(color);
    dialog.setDirectories(paths);
    dialog.setRecursive(recursive);
    dialog.setInterval(interval);
    dialog.setScreenConfig(screens, hosts);
    dialog.setClientConfig(clientHost, clientPort);
    dialog.setServerConfig(serverPort);
}

//void Walle::readSettingsAndApply()
//{
//    QSettings settings;

////    qDebug() << settings.fileName();

//    settings.beginGroup("settings");
//    QStringList paths(settings.value("paths").toStringList());
//    bool recursive = settings.value("recursive", false).toBool();
//    settings.endGroup();
//    settings.beginGroup("other");
//    int interval = settings.value("interval", 180).toInt();
//    settings.endGroup();
//    settings.beginGroup("wallpaper");
//    QColor color(settings.value("color", QColor(Qt::white)).toString());
//    settings.endGroup();


//    WTypes::ScreenList screens;
//    WTypes::HostList hosts;

//    settings.beginGroup("screens");
//    int size = settings.beginReadArray("screen");
//    for (int s = 0; s < size; s++)
//    {
//        settings.setArrayIndex(s);
//        QRect actual = settings.value("actual").toRect();
//        QRect tweaked = settings.value("tweaked").toRect();

//        WTypes::ScreenConfig screen(actual, tweaked);
//        screens << screen;
//    }
//    settings.endArray();
//    size = settings.beginReadArray("host");
//    for (int h = 0; h < size; h++)
//    {
//        settings.setArrayIndex(h);

//        WTypes::HostConfig host;
//        host.mode = WTypes::NumberOfWallpapers(settings.value("mode").toUInt());
//        QStringList screenstrings = settings.value("screens").toStringList();
//        for (int s = 0; s < screenstrings.size(); s++)
//            host.screens << screenstrings.at(s).toInt();
//        hosts << host;
//    }
//    settings.endArray();
//    settings.endGroup();


//    if (timer.interval() != interval*1000)
//    {
//        timer.setInterval(interval*1000);
//        timer.start();
//    }

//    emit pathsChanged(paths, recursive);
//    emit backgroundColorChanged(color);
//    emit changeScreenConfig(screens, hosts);

////    dialog.setColor(color);
////    dialog.setDirectories(paths);
////    dialog.setRecursive(recursive);
////    dialog.setInterval(interval);
////    dialog.setScreenConfig(screens, hosts);
//}

void Walle::getAndWriteSettings()
{
    QSettings settings;

    QStringList paths(dialog.getDirectories());
    bool recursive(dialog.getRecursive());
    int interval(dialog.getInterval());
    QColor color(dialog.getColor());
    paths.removeDuplicates();

    WTypes::ScreenList screens;
    WTypes::HostList hosts;
    dialog.getScreenConfig(&screens, &hosts);

    QString clientHost;
    quint16 clientPort;
    dialog.getClientConfig(&clientHost, &clientPort);

    quint16 serverPort(dialog.getServerConfig());

    settings.beginGroup("settings");
    settings.setValue("paths", paths);
    settings.setValue("recursive", recursive);
    settings.endGroup();
    settings.beginGroup("server");
    settings.setValue("interval", interval);
    settings.setValue("port", serverPort);
    settings.endGroup();
    settings.beginGroup("client");
    settings.setValue("host", clientHost);
    settings.setValue("port", clientPort);
    settings.endGroup();
    settings.beginGroup("wallpaper");
    settings.setValue("color", color.name());
    settings.endGroup();

    writeScreenConfig(screens, hosts, true);

//    settings.beginGroup(QString("hostconfiguration-%1").arg(currentHostConfig.join("_")));
//    settings.beginWriteArray("screen");
//    for (int screen = 0; screen < screens.size(); screen++)
//    {
//        settings.setArrayIndex(screen);
//        settings.setValue("actual", screens.at(screen).actual);
//        settings.setValue("tweaked", screens.at(screen).tweaked);
//    }
//    settings.endArray();
//    settings.beginWriteArray("host");
//    for (int host = 0; host < hosts.size(); host++)
//    {
//        settings.setArrayIndex(host);
//        QStringList screens;
//        foreach (int screen, hosts.at(host).screens)
//            screens << QString("%1").arg(screen);
//        settings.setValue("screens", screens);
//        settings.setValue("mode", hosts.at(host).mode);
//        settings.setValue("hostname", hosts.at(host).hostname);
//    }
//    settings.endArray();
//    settings.endGroup();
}
