#include "networkclient.h"

#include "systemintegration.h"

#include <QVector>
#include <QBuffer>
#include <QTimer>

NetworkClient::NetworkClient(QObject *parent) :
    QObject(parent),
    m_socket(NULL)
{
}

void NetworkClient::onConnected()
{
    qDebug() << "connected";
    QDataStream stream(m_socket);

    //    QDesktopWidget widget;

    //    QVector<QRect> screens;
    //    for (int s = 0; s < widget.screenCount(); s++)
    //        screens << widget.screenGeometry(s);

    qDebug() << m_screens;
    stream << QString("hostname");
    stream << QHostInfo::localHostName();
    stream << QString("mode");
    stream << (uint)SystemIntegration::getMode();
    stream << QString("screens");
    stream << m_screens;
    //    QTimer::singleShot(1000, this, SLOT(changeWallpaper()));
}

QImage NetworkClient::readImage()
{
    QDataStream stream(m_socket);
    QByteArray buffer;
    QImage image;			      // Construct a new QImage
    int bytesCount, bytesCount2;
    QString header;
    while (m_socket->isReadable() && m_socket->atEnd())
        m_socket->waitForReadyRead(1000);
    stream >> header;
    if (header != "image")
        return image;
    while (m_socket->isReadable() && m_socket->atEnd())
        m_socket->waitForReadyRead(1000);
    stream >> bytesCount;
    qDebug() << bytesCount << "bytecount";
    while (m_socket->isReadable() && m_socket->atEnd())
        m_socket->waitForReadyRead(1000);
    stream >> bytesCount2;
    qDebug() << bytesCount2 << "bytecount";
    if (bytesCount != bytesCount2)
        return QImage();
    while (buffer.size() < bytesCount)
    {
        quint64 bytesLeft = bytesCount - buffer.size();
        qDebug() << bytesLeft << "bytes left";
        while (m_socket->isReadable() && m_socket->atEnd())
            m_socket->waitForReadyRead(1000);
        buffer.append(m_socket->read(bytesLeft));
        qDebug() << "managed to read" << buffer.size() << "bytes in total";
    }
    qDebug() << "buffer contains" << buffer.size() << "bytes";

    image.loadFromData(buffer); // Load the image from the receive buffer
    if (image.isNull())		      // Check if the image was indeed received
        qDebug("The image is null. Something failed.");
    return image;
}

void NetworkClient::onReadyRead()
{
    QDataStream stream(m_socket);

    while (m_socket->bytesAvailable() > 0)
    {
        QString header;
        stream >> header;

        if (header == "images")
        {
            QVector<QImage> images;

            int numimages;
            while (m_socket->isReadable() && m_socket->atEnd())
                m_socket->waitForReadyRead(1000);
            stream >> numimages;
            qDebug() << "Receiving images" << numimages;
            for (int i = 0; i < numimages; i++)
            {
                images << readImage();
            }
            qDebug() << "Received images" << images.size();
            SystemIntegration::applyWallpapers(images);
        }
        else if (header == "wallpapers")
        {
            QVector<WTypes::Wallpaper> wallpapers;

            int numimages;
            while (m_socket->isReadable() && m_socket->atEnd())
                m_socket->waitForReadyRead(1000);
            stream >> numimages;
            qDebug() << "Receiving wallpapers" << numimages;
            for (int i = 0; i < numimages; i++)
            {
                WTypes::Wallpaper wp;
                while (m_socket->isReadable() && m_socket->atEnd())
                    m_socket->waitForReadyRead(1000);
                stream >> wp.image.path;
                wp.thumbnail = readImage();
                wallpapers << wp;
                qDebug() << "Received wallpaper" << wp.image.path << wp.thumbnail.size();
            }
            emit wallpapersChanged(wallpapers);
        }
        else if (header == "changing")
        {
            qDebug() << "Server is changing wps";
            emit changingWallpapers();
        }
        else
        {
            qDebug() << "Unknown header" << header;
//            qDebug() << "data" << m_socket->readAll();
        }
    }
}

void NetworkClient::onDisconnected()
{
    qDebug() << "disconnected";
}

void NetworkClient::onError(QAbstractSocket::SocketError error)
{
    switch (error)
    {
    case QAbstractSocket::ConnectionRefusedError:
    case QAbstractSocket::RemoteHostClosedError:
    case QAbstractSocket::HostNotFoundError:
    case QAbstractSocket::SocketResourceError:
    case QAbstractSocket::SocketTimeoutError:
    case QAbstractSocket::NetworkError:
        qDebug() << "error" << error << m_socket->errorString();
        qDebug() << "will try to reconnect";
        QTimer::singleShot(30000, this, SLOT(reconnect()));
        break;
    default:
        qDebug() << "error" << error << m_socket->errorString();
    }
}

void NetworkClient::reconnect()
{
    connectToHost(lastAddress, lastPort);
}

void NetworkClient::connectToHost(QString address, quint16 port)
{
    if (m_socket == NULL)
    {
        m_socket = new QTcpSocket();
        connect(m_socket, SIGNAL(connected()), this, SLOT(onConnected()));
        connect(m_socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
        connect(m_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
        connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
    }

    if (address == lastAddress && port == lastPort)
        return;

    m_socket->disconnectFromHost();

    qDebug() << "connecting to" << address << "on port" << port;
    m_socket->connectToHost(address, port);

    lastAddress = address;
    lastPort = port;

    //    socket.waitForConnected();
    //    qDebug() << "done." << socket.isValid() << socket.state();
}

void NetworkClient::setScreens(QVector<QRect> screens)
{
    m_screens = screens;
}

void NetworkClient::changeWallpaper()
{
    QDataStream stream(m_socket);
    stream << QString("change");
}

void NetworkClient::deleteWallpaper(QString path)
{
    QDataStream stream(m_socket);
    stream << QString("delete");
    stream << path;
}
