#include "imagerepository.h"

#include <QDebug>
#include <QDir>
#include <QImageReader>
#include <QTime>

ImageRepository::ImageRepository(QObject *parent) :
    QObject(parent)
{
    connect(&fswatcher, SIGNAL(directoryChanged(QString)),
            this, SLOT(scanDirectory(QString)));
}

bool ImageRepository::getImage(QList<QSize> sizes, QStringList excludeImages, WTypes::Image *image) const
{
    ImageList images = filterImages(sizes, excludeImages);

    qDebug() << images.size() << "images matches" << sizes;

    if (images.size() == 0)
    {
        qDebug() << "Not returning new wallpaper, images.size():" << images.size();
        return false;
    }

    int random = qrand() % images.size();

    *image = images.at(random);

//    qDebug() << "got new wp!" << image->path;
    return true;
}

ImageRepository::ImageList ImageRepository::filterImages(QList<QSize> sizes, QStringList excludeImages) const
{
    ImageList images;

    QHashIterator<QString, WTypes::Image> i(files);
    while (i.hasNext()) {
        i.next();

        if (excludeImages.contains(i.value().path))
            continue;

//        QSize size = i.value().imageSize;
//        if (size.isEmpty())
//        {
//            reader.setFileName(i.value().path);
//            size = reader.size();
//            i.value().imageSize = size;
//            qDebug() << i.value().path << i.value().imageSize;
//        }

        WTypes::Image img = i.value();
        if (matchSizes(sizes, &img))
        {
            images.append(img);
        }
    }
    return images;
}

bool ImageRepository::matchSizes(QList<QSize> sizes, WTypes::Image *image) const
{
    QList<QSize> matchingSizes;
    foreach (QSize size, sizes)
    {
        if (matchSize(size, image->imageSize))
        {
            matchingSizes.append(size);
        }
    }
    if (matchingSizes.empty())
        return false;

    image->imageSize = matchingSizes.at(0);
//    int random = qrand() % matchingSizes.size();

//    image->imageSize = matchingSizes.at(random);
    return true;
}

bool ImageRepository::matchSize(QSize screenSize, QSize imageSize) const
{
    return (qAbs((qreal)screenSize.width()/(qreal)screenSize.height() - (qreal)imageSize.width()/(qreal)imageSize.height()) < (qreal).2) &&
            ((qreal)imageSize.width() / (qreal)screenSize.width() > (qreal).8 && (qreal)imageSize.height() / (qreal)screenSize.height() > (qreal).8) /*&& screenSize != imageSize*/;
}

void ImageRepository::setDirectories(QStringList paths, bool recursive)
{
    qsrand((uint)QTime::currentTime().msec());
    if (paths == watchedPaths && recursive == recursiveScan)
        return;

    recursiveScan = recursive;
    watchedPaths = paths;

    files.clear();

    if (!fswatcher.directories().empty())
        fswatcher.removePaths(fswatcher.directories());

    foreach(QString p, paths)
    {
        scanDirectory(p);
    }
    qDebug() << "found" << files.size() << "images";
}

void ImageRepository::scanDirectory(QString path)
{
    qDebug() << "scanning path:" << path;
    QImageReader reader;
    QDir dir;
    dir.setPath(path);
    if (recursiveScan)
    {
        dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

        foreach(QFileInfo subdir, dir.entryInfoList())
        {
            scanDirectory(subdir.filePath());
        }
    }

    dir.setFilter(QDir::Files | QDir::Readable);
    QStringList nameFilters;
    foreach (QByteArray b, QImageReader::supportedImageFormats())
        nameFilters << "*." + b;
    dir.setNameFilters(nameFilters);

    files.remove(path);

    foreach(QFileInfo file, dir.entryInfoList())
    {
        WTypes::Image img(file.filePath());

        reader.setFileName(img.path);
        img.imageSize = reader.size();
        files.insert(path, img);
    }

    if (!fswatcher.directories().contains(path))
        fswatcher.addPath(path);
}

