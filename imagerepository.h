#ifndef IMAGEREPOSITORY_H
#define IMAGEREPOSITORY_H

#include "types.h"

#include <QObject>
#include <QStringList>
//#include <QList>
#include <QSize>
//#include <QImage>
#include <QMultiHash>
#include <QFileSystemWatcher>

class ImageRepository : public QObject
{
    Q_OBJECT
public:

    typedef QMultiHash<QString, WTypes::Image> DirectoryList;
    typedef QList<WTypes::Image> ImageList;

    explicit ImageRepository(QObject *parent = 0);

    bool getImage(QList<QSize> sizes, QStringList excludeImages, WTypes::Image *image) const;

private:
    ImageList filterImages(QList<QSize> sizes, QStringList excludeImages) const;
    bool matchSizes(QList<QSize> sizes, WTypes::Image *image) const;
    bool matchSize(QSize size, QSize size2) const;

private slots:
    void scanDirectory(QString path);

signals:

public slots:
    void setDirectories(QStringList paths, bool recursiveScan);
//    void setRatioTolerance();
//    void setSizeTolerance();

private:
    DirectoryList files;
    QFileSystemWatcher fswatcher;
    bool recursiveScan;
    QStringList watchedPaths;

};

#endif // IMAGEREPOSITORY_H
