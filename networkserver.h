#ifndef NETWORKSERVER_H
#define NETWORKSERVER_H

#include "types.h"

#include <QObject>
#include <QImage>
#include <QVector>
#include <QTcpServer>
#include <QStringList>

class QTcpSocket;

class RemoteClient : public QObject
{
    Q_OBJECT
public:
    explicit RemoteClient(QTcpSocket *socket, QObject *parent = 0);
    bool isReady();
    const QVector<QRect> &screens();
    const QString &hostname();
    WTypes::NumberOfWallpapers mode();
    void sendImages(QVector<QImage> hostImages, QVector<WTypes::Wallpaper> newWallpapers);
    void notify(QString message);

private:
    void sendImage(const QImage &image);

signals:
    void disconnectMe(RemoteClient* me);
    void ready();
    void changeWallpaper();

private slots:
    void onReadyRead();
    void onDisconnect();

public slots:

private:
    WTypes::NumberOfWallpapers m_wallpaperMode;
    QVector<QRect> m_screens;
    QString m_hostname;
    QTcpSocket* m_socket;
};

class WallpaperServer : public QObject
{
    Q_OBJECT
public:
    explicit WallpaperServer(QObject *parent = 0);

//    unsigned int getNumberOfHosts() const;
//    void applyWallpapersForHost(QVector<QImage> images, unsigned int host) const;
    void applyWallpapers(QVector<QVector<QImage> > hostImages, QVector<WTypes::Wallpaper> newWallpapers) const;
    void notifyClients(QString message) const;
signals:
    void hostConfigChanged(WTypes::ScreenList screens, WTypes::HostList hosts);
    void changeWallpaper();
    void listeningOnPort(quint16 port);

private slots:
    void removeConnection(RemoteClient *object);
    void onNewConnection();

public slots:
    void reloadScreenConfigurations();
    void listenOnPort(quint16 port);

private:
    QTcpServer *tcpserver;

    QVector<RemoteClient*> clients;

};

#endif // NETWORKSERVER_H
