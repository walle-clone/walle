#include "screengraphicsitem.h"

#include <QPainter>
#include <QFont>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>

ScreenGraphicsItem::ScreenGraphicsItem(const QString &name, const QRect &rect, QGraphicsItem *parent) :
    QGraphicsRectItem(QRect(QPoint(0,0), rect.size()), parent),
    m_name(name)
{
//    setFlag(QGraphicsItem::ItemIsMovable);
}

void ScreenGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem */*option*/,
                               QWidget */*widget*/)
{
    painter->setFont(QFont("Helvetica", 100));
    painter->fillRect(rect(), QColor(0, 0, 200, 100));
    painter->setPen(QPen(Qt::black, 10));
    painter->drawText(rect(), Qt::AlignCenter, m_name);
}

void ScreenGraphicsItem::setScreenRect(QRect r)
{
//    qDebug() << m_name << pos().toPoint() << scenePos().toPoint() << rect().toRect() << sceneBoundingRect().toRect() << boundingRect().toRect();
    setPos(r.x(), r.y());
    setRect(rect().x(), rect().y(), r.width(), r.height());
}

void ScreenGraphicsItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    emit screenRectChanged(sceneBoundingRect().toRect());
//    setRect(rect());
//    qDebug() << event->lastPos().toPoint() << event->pos().toPoint();
//    qDebug() << m_name << sceneBoundingRect().toRect();
    QGraphicsRectItem::mouseMoveEvent(event);
}
