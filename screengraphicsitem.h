#ifndef SCREENGRAPHICSITEM_H
#define SCREENGRAPHICSITEM_H

#include <QObject>
#include <QGraphicsRectItem>

class ScreenGraphicsItem : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    explicit ScreenGraphicsItem(const QString &name, const QRect &rect, QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/ = 0);
private:
    QString m_name;
    
signals:
    void screenRectChanged(QRect rect);
    
public slots:
    void setScreenRect(QRect r);

protected:
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    
};

#endif // SCREENGRAPHICSITEM_H
