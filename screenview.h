#ifndef SCREENVIEW_H
#define SCREENVIEW_H

#include <QGraphicsView>

class ScreenView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit ScreenView(QWidget *parent = 0);

protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
signals:
    
public slots:
    void refitScene();
    
};

#endif // SCREENVIEW_H
