#ifndef REMOTECLIENT_H
#define REMOTECLIENT_H

#include <QObject>

#include <QTcpSocket>
#include <QHostAddress>
#include <QHostInfo>
#include <QVector>
#include <QRect>

#include "types.h"

class NetworkClient : public QObject
{
    Q_OBJECT
public:
    explicit NetworkClient(QObject *parent = 0);

private:
    QImage readImage();

signals:
    void wallpapersChanged(QVector<WTypes::Wallpaper> wallpapers);
    void changingWallpapers();
    
private slots:
    void onReadyRead();
    void onConnected();
    void onDisconnected();
    void onError(QAbstractSocket::SocketError error);
    void reconnect();

public slots:
    void connectToHost(QString address, quint16 port);
    void setScreens(QVector<QRect> screens);
    void changeWallpaper();
    void deleteWallpaper(QString path);

private:
    QTcpSocket *m_socket;

    QString lastAddress;
    quint16 lastPort;
    QVector<QRect> m_screens;
    
};

#endif // REMOTECLIENT_H
