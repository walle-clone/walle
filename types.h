#ifndef TYPES_H
#define TYPES_H

#include <QList>
#include <QRect>
#include <QImage>

namespace WTypes {


struct Image {
    Image() {}
    Image(QString p) : path(p) {}
    QString path;
    QSize imageSize;
};

struct ScreenConfig {
    ScreenConfig(QRect actualConfig) :
        actual(actualConfig) {}
    ScreenConfig(QRect actualConfig, QRect tweakedConfig) :
        actual(actualConfig), tweaked(tweakedConfig) {}
    QRect actual, tweaked;
};

typedef QList<ScreenConfig> ScreenList;
typedef QList<ScreenConfig*> ScreenRefList;

struct ScreenSize {
    QVector<int> screens;
    QSize size;
};

enum ReturnValue {
    Success,
    UnsupportedDesktopEnvironment,
    ReadError,
    WriteError,
    UnknownDesktopEnvironment,
    NoScreensDetected,
    SkipImage
};

enum NumberOfWallpapers {
    OnePerScreen,
    JustOne,
    NoWallpaper
};

struct HostConfig {
    QVector<int> screens;
    QString hostname;
    NumberOfWallpapers mode;
};

typedef QVector<HostConfig > HostList;

struct Wallpaper {
    QVector<int> screens;
    Image image;
    QImage thumbnail;
};

typedef QVector<ScreenSize> ScreenSizeList;
typedef QVector<QSize> SizeList;

static int thumbNailSize = 256;

}

#endif // TYPES_H
