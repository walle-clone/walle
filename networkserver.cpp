#include "networkserver.h"

#include <QTcpSocket>
#include <QBuffer>
#include <QFile>

WallpaperServer::WallpaperServer(QObject *parent) :
    QObject(parent),
    tcpserver(NULL)
{
}

//unsigned int WallpaperServer::getNumberOfHosts() const
//{
//    return 0;
//}

//void WallpaperServer::applyWallpapersForHost(QVector<QImage> images, unsigned int host) const
//{
//}

void WallpaperServer::applyWallpapers(QVector<QVector<QImage> > hostImages,
                                      QVector<WTypes::Wallpaper> newWallpapers) const
{
    for (int host = 0; host < clients.size(); host++)
    {
        //        if (host == 0)
        //        {
        //            SystemIntegration::applyWallpapers(hostImages.at(host));
        //        }
        //        else
        //        {
        if (clients.at(host)->isReady())
        {
            qDebug() << "Applying" << hostImages.at(host).size() << "images for host" << host;
            clients.at(host)->sendImages(hostImages.at(host), newWallpapers);
        }
        //            remote->applyWallpapersForHost(hostImages.at(host), host);
        //        }
    }
}

void WallpaperServer::notifyClients(QString message) const
{
    for (int host = 0; host < clients.size(); host++)
    {
        if (clients.at(host)->isReady())
            clients.at(host)->notify(message);
    }
}

void WallpaperServer::reloadScreenConfigurations()
{
    qDebug() << "reloading screen config";
    WTypes::ScreenList screens;
    WTypes::HostList hosts;

    for (int i = 0; i < clients.size(); i++)
    {
        if (clients.at(i)->isReady())
        {
            const QVector<QRect> &hostScreens = clients.at(i)->screens();
            WTypes::HostConfig hostcfg;
            for (int s = 0; s < hostScreens.size(); s++)
            {
                hostcfg.screens << screens.size();
                screens << WTypes::ScreenConfig(hostScreens.at(s));
            }
            hostcfg.mode = clients.at(i)->mode();
            hostcfg.hostname = clients.at(i)->hostname();
            hosts << hostcfg;
            qDebug() << "Client" << clients.at(i)->hostname() << "(" << i << "):" << clients.at(i)->screens();
        }
    }
    emit hostConfigChanged(screens, hosts);
}

void WallpaperServer::removeConnection(RemoteClient *object)
{
    qDebug() << "removing connection";
    int index = clients.indexOf(object);
    if (index >= 0)
    {
        clients.remove(index);
        reloadScreenConfigurations();
    }
}

void WallpaperServer::onNewConnection()
{
    qDebug() << "got new connection";
    while (tcpserver->hasPendingConnections())
    {
        QTcpSocket *socket = tcpserver->nextPendingConnection();
        RemoteClient *client = new RemoteClient(socket);

        connect(socket, SIGNAL(readyRead()), client, SLOT(onReadyRead()));
        connect(socket, SIGNAL(disconnected()), client, SLOT(onDisconnect()));
        connect(client, SIGNAL(disconnectMe(RemoteClient*)),
                this, SLOT(removeConnection(RemoteClient*)));
        connect(client, SIGNAL(ready()),
                this, SLOT(reloadScreenConfigurations()));
        connect(client, SIGNAL(changeWallpaper()),
                this, SIGNAL(changeWallpaper()));

        qDebug() << "connection from" << socket->peerName() << socket->peerPort();
        //        socket->write("apa");
        clients.append(client);
    }
    for (int i = 0; i < clients.size(); i++)
    {
        for (int j = i; j < clients.size(); j++)
        {
            if (clients[j]->hostname().compare(clients[j]->hostname()) < 0)
            {
                RemoteClient *c = clients[i];
                clients[i] = clients[j];
                clients[j] = c;
            }
        }
    }
}

void WallpaperServer::listenOnPort(quint16 port)
{
    if (tcpserver == NULL)
    {
        tcpserver = new QTcpServer();
        connect(tcpserver, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    }

    if (tcpserver->isListening() && (tcpserver->serverPort() == port || port == 0))
        return;

    tcpserver->close();

    if (!tcpserver->listen(QHostAddress::Any, port))
        qDebug() << "could not listen on port" << tcpserver->errorString();
    else
    {
        qDebug() << "listyting on" << tcpserver->serverPort();
        emit listeningOnPort(tcpserver->serverPort());
    }
}


RemoteClient::RemoteClient(QTcpSocket *socket, QObject *parent) :
    QObject(parent),
    m_socket(socket)
{
}

bool RemoteClient::isReady()
{
    return !m_hostname.isEmpty() && !m_screens.isEmpty();
}

const QVector<QRect>& RemoteClient::screens()
{
    return m_screens;
}

const QString& RemoteClient::hostname()
{
    return m_hostname;
}

WTypes::NumberOfWallpapers RemoteClient::mode()
{
    return m_wallpaperMode;
}

void RemoteClient::sendImage(const QImage &image)
{
    QDataStream stream(m_socket);

    QByteArray bytes;
    QBuffer buf(&bytes);
    buf.open(QIODevice::WriteOnly);
    if (!image.save(&buf, "png"))
        qDebug() << "could not save";
//    m_socket->flush();
//    qDebug() << "will write" << bytes.size() << "bytes";
    stream << QString("image");
    stream << bytes.size();
    stream << bytes.size();
//    m_socket->flush();
    /*quint64 bytesWritten = */m_socket->write(bytes);
//    qDebug() << "wrote" << bytesWritten << "bytes";
//    m_socket->flush();
}

void RemoteClient::sendImages(QVector<QImage> hostImages,
                                       QVector<WTypes::Wallpaper> newWallpapers)
{
    QDataStream stream(m_socket);

    stream << QString("images");
    stream << hostImages.size();
    foreach (const QImage &img, hostImages)
        sendImage(img);

    stream << QString("wallpapers");
    stream << newWallpapers.size();
    foreach (const WTypes::Wallpaper &wp, newWallpapers)
    {
        stream << wp.image.path;
        sendImage(wp.thumbnail);
    }
}

void RemoteClient::notify(QString message)
{
    QDataStream stream(m_socket);
    stream << message;
}

void RemoteClient::onReadyRead()
{
    QDataStream stream(m_socket);

    while (m_socket->bytesAvailable() > 0)
    {
        QString header;
        stream >> header;

        if (header == "screens")
        {
    //        QVector<QRect> screens;
            stream >> m_screens;
//            qDebug() << m_screens;
        }
        else if (header == "hostname")
        {
            stream >> m_hostname;
//            qDebug() << m_hostname;
        }
        else if (header == "mode")
        {
            uint wm;
            stream >> wm;
            m_wallpaperMode = WTypes::NumberOfWallpapers(wm);
//            qDebug() << m_wallpaperMode;
        }
        else if (header == "change")
        {
            qDebug() << "wallpaper change requested";
            emit changeWallpaper();
        }
        else if (header == "delete")
        {
            QString path;
            stream >> path;
            qDebug() << "wallpaper deletion requested" << path;

            QFile file(path);
            if (!file.remove())
                qDebug() << file.errorString();
            else
            {
                qDebug() << "wallpaper deleted" << path;
//                emit changeWallpaper();
            }
        }
        else
        {
            qDebug() << "Unknown header" << header;
//            qDebug() << "data" << m_socket->readAll();
        }
    }

    if (isReady())
        emit ready();
}

void RemoteClient::onDisconnect()
{
    qDebug() << "disconnect";
    m_screens.clear();
    emit disconnectMe(this);
}
