#ifndef WALLPAPERCHANGER_H
#define WALLPAPERCHANGER_H

#include "imagerepository.h"
#include "networkserver.h"
#include "types.h"

#include <QObject>
#include <QRect>
#include <QStringList>
#include <QVector>
#include <QColor>

class WallpaperChanger : public QObject
{
    Q_OBJECT
public:
    explicit WallpaperChanger(const ImageRepository *repository = 0,
                              const WallpaperServer *remote = 0,
                              QObject *parent = 0);

private:
    QVector<WTypes::Wallpaper> getNewWallpapers() const;
    QVector<QImage> createNewWallpapers(QVector<WTypes::Wallpaper> &newWallpapers) const;
    QVector<QVector<QImage> > prepareWallpapersForHosts(const QVector<QImage> images) const;
    void svgWorkAround(const QString path, QImage *image) const;

    void createExcludeFilter(const QVector<WTypes::Wallpaper> &wallpapers);

public slots:
    void changeBackgroundColor(QColor color);
    void changeWallpaper();
    void changeScreenConfig(WTypes::ScreenList newConfig, WTypes::HostList hostConfig);

private:
    const ImageRepository *repo;
    const WallpaperServer *remote;
    WTypes::ScreenList m_screenConfigurations;
    WTypes::ScreenSizeList m_screenSizes;
    WTypes::HostList hosts;
    QColor backGroundcolor;
    QStringList excludePaths;
};

Q_DECLARE_METATYPE(WTypes::ScreenList)
Q_DECLARE_METATYPE(WTypes::HostList)
Q_DECLARE_METATYPE(WTypes::Wallpaper)

#endif // WALLPAPERCHANGER_H
