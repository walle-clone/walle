#include "configdialog.h"
#include "ui_configdialog.h"

#include "screengraphicsitem.h"

#include <QFileDialog>
#include <QDebug>
#include <QAbstractButton>
#include <QColorDialog>
#include <QPixmap>
#include <QMessageBox>
#include <QWidgetAction>

ConfigDialog::ConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDialog)
{
    ui->setupUi(this);
    ui->directoryView->setModel(&model);

    subMenu = traymenu.addMenu(
                QIcon::fromTheme("edit-delete",
                                 QIcon(":/icons/edit-delete.svgz")),
                "Delete wallpaper");

    traymenu.addAction(ui->actionChange_wallpaper);
    traymenu.addAction(ui->actionOpen_configuration);
    traymenu.addAction(ui->actionQuit);

    trayicon.setContextMenu(&traymenu);
    normalIcon = QIcon::fromTheme("preferences-desktop-wallpaper",
                                  QIcon(":/icons/preferences-desktop-wallpaper.svgz"));
    busyIcon = QIcon::fromTheme("view-refresh",
                                  QIcon(":/icons/view-refresh.svgz"));
    trayicon.setIcon(normalIcon);
    trayicon.show();

    ui->screenView->setScene(&screenScene);

    connect(&deleteMapper, SIGNAL(mapped(int)),
            this, SLOT(deleteWallpaper(int)));

    connect(ui->buttonBox->button(QDialogButtonBox::Apply),
            SIGNAL(clicked()), this, SIGNAL(apply()));

    connect(ui->actionChange_wallpaper, SIGNAL(triggered()),
            this, SIGNAL(changeWallpaper()));
    connect(ui->actionOpen_configuration, SIGNAL(triggered()),
            this, SIGNAL(openConfig()));
    connect(ui->actionQuit, SIGNAL(triggered()),
            this, SIGNAL(quit()));
    connect(ui->nextButton, SIGNAL(clicked()),
            this, SIGNAL(changeWallpaper()));
}

ConfigDialog::~ConfigDialog()
{
    delete ui;
}

void ConfigDialog::setDirectories(QStringList paths)
{
    model.setStringList(paths);
}

QStringList ConfigDialog::getDirectories()
{
    return model.stringList();
}

void ConfigDialog::setInterval(unsigned int interval)
{
    ui->intervalEdit->setTime(QTime().addSecs(interval));
}

unsigned int ConfigDialog::getInterval()
{
    return QTime().secsTo(ui->intervalEdit->time());
}

void ConfigDialog::setRecursive(bool recursive)
{
    ui->recusiveCheckbox->setChecked(recursive);
}

bool ConfigDialog::getRecursive()
{
    return ui->recusiveCheckbox->isChecked();
}

void ConfigDialog::setScreenConfig(WTypes::ScreenList screenConfig, WTypes::HostList hostConfig)
{
    temp_screenConfig = screenConfig;
    temp_hostConfig = hostConfig;

    screenScene.clear();

    foreach (ScreenRectConfig *src, screenRectConfigs)
    {
        ui->screenConfig->layout()->removeWidget(src);
        delete src;
    }
    screenRectConfigs.clear();

//    QLayoutItem* item;
//    while ( ( item = ui->screenConfig->layout()->takeAt( 0 ) ) != NULL )
//    {
//        delete item->widget();
//        delete item;
//    }

    for (int i = 0; i < screenConfig.size(); i++)
    {
        const WTypes::ScreenConfig &sc = screenConfig.at(i);
        QString name("%1-%2");
        foreach (const WTypes::HostConfig &hc, hostConfig)
        {
            if (hc.screens.contains(i))
                name = name.arg(hc.hostname).arg(hc.screens.indexOf(i));
        }
//        qDebug() << name << sc.tweaked;
        ScreenGraphicsItem *item = new ScreenGraphicsItem(name, sc.tweaked);
        item->setPos(sc.tweaked.topLeft());
        ScreenRectConfig *cfg = new ScreenRectConfig(sc.tweaked);
        connect(cfg, SIGNAL(valueChanged(QRect)), item, SLOT(setScreenRect(QRect)));
        connect(item, SIGNAL(screenRectChanged(QRect)), cfg, SLOT(setValue(QRect)));
        connect(cfg, SIGNAL(valueChanged(QRect)), ui->screenView, SLOT(refitScene()));
        screenRectConfigs.append(cfg);
        screenScene.addItem(item);
        ui->screenConfig->layout()->addWidget(cfg);
    }
//    ui->screenView->ensureVisible(screenScene.itemsBoundingRect());
}

void ConfigDialog::getScreenConfig(WTypes::ScreenList *screenConfig, WTypes::HostList *hostConfig)
{
    *screenConfig = temp_screenConfig;
    *hostConfig = temp_hostConfig;

    for (int i = 0; i < screenConfig->size(); i++)
//    {
        (*screenConfig)[i].tweaked = screenRectConfigs.at(i)->getScreenRect();
//        qDebug() << screenConfig->at(i).tweaked;
//    }
}

void ConfigDialog::setColor(QColor color)
{
    ui->colorButton->setToolTip(color.name());
    ui->colorButton->setStyleSheet(QString("background-color: %1").arg(color.name()));
}

QColor ConfigDialog::getColor()
{
    return QColor(ui->colorButton->toolTip());
}

void ConfigDialog::setClientConfig(QString clientHost, quint16 clientPort)
{
    ui->clientAddressEdit->setText(clientHost);
    ui->clientPortSpinBox->setValue(clientPort);
}

void ConfigDialog::getClientConfig(QString *clientHost, quint16 *clientPort)
{
    *clientHost = ui->clientAddressEdit->text();
    *clientPort = ui->clientPortSpinBox->value();
}

void ConfigDialog::setServerConfig(quint16 serverPort)
{
    ui->serverPortSpinBox->setValue(serverPort);
}

quint16 ConfigDialog::getServerConfig()
{
    return ui->serverPortSpinBox->value();
}

void ConfigDialog::addDirectory()
{
    QString path = QFileDialog::getExistingDirectory(this,
                                                     tr("Select directory"),
                                                     lastPath);

    if (!path.isNull() && !model.stringList().contains(path))
    {
        lastPath = path;
//        qDebug() << "selected: " << path;
        model.insertRows(model.rowCount(), 1);
        model.setData(model.index(model.rowCount()-1), path);
    }
}

void ConfigDialog::removeDirectory()
{
    QModelIndexList indices;
    while((indices = ui->directoryView->selectionModel()->selectedIndexes()).size())
        model.removeRow(indices.first().row());
}

void ConfigDialog::chooseColor()
{
    QColor color = QColorDialog::getColor(Qt::white, this);
    setColor(color);
}

void ConfigDialog::onWallpapersChanged(QVector<WTypes::Wallpaper> wallpapers)
{
    subMenu->clear();
//    subMenu->setStyleSheet("QMenu::item { min-height: 128px; width: 128px; } ");
    subMenu->setStyleSheet("QMenu::item:selected { spacing: 20px; padding:20px; } ");
    for (int i = 0; i < wallpapers.size(); i++)
    {
        if (i != 0)
            subMenu->addSeparator();
        const WTypes::Wallpaper &wp = wallpapers.at(i);
        qDebug() << "CONFDIALOG GOT" << wp.image.path;
        QLabel *label = new QLabel(this);
        label->setPixmap(QPixmap::fromImage(wp.thumbnail));

        QWidgetAction *waction = new QWidgetAction(this);
        waction->setDefaultWidget(label);
        subMenu->addAction(waction);

        connect(waction, SIGNAL(triggered()), &deleteMapper, SLOT(map()));
        deleteMapper.setMapping(waction, i);
    }
    cur_wallpapers = wallpapers;
    trayicon.setIcon(normalIcon);
    ui->actionChange_wallpaper->setEnabled(true);
    ui->nextButton->setEnabled(true);
}

void ConfigDialog::onWallpapersChanging()
{
    trayicon.setIcon(busyIcon);
    ui->actionChange_wallpaper->setEnabled(false);
    ui->nextButton->setEnabled(false);
}

void ConfigDialog::deleteWallpaper(int index)
{
    if (index >= cur_wallpapers.size())
    {
        qDebug() << "fail";
        return;
    }
    QString path(cur_wallpapers.at(index).image.path);

    QMessageBox msgBox;
    msgBox.setAttribute(Qt::WA_QuitOnClose, false);

    msgBox.setIconPixmap(QPixmap::fromImage(cur_wallpapers.at(index).thumbnail));
    msgBox.setText(tr("This will delete %1 permanently.").arg(path));
    msgBox.setInformativeText("Are you sure?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Ok)
    {
        emit deleteWallpaper(path);
    }
}
