#ifndef WALLE_H
#define WALLE_H

#include "imagerepository.h"
#include "wallpaperchanger.h"
#include "networkserver.h"
#include "networkclient.h"
#include "types.h"
#include "configdialog.h"

#include <QObject>
#include <QTimer>
#include <QThread>

class QApplication;

class Walle : public QObject
{
    Q_OBJECT
public:
    explicit Walle(QApplication *a);
    
signals:
    void requestWallpaperChanged();
    void changeWallpaper();
    void connectToHost(QString address, quint16 port);
    void listenOnPort(quint16 port);
    void backgroundColorChanged(QColor color);
    void pathsChanged(QStringList paths, bool recursive);
    void changeScreenConfig(WTypes::ScreenList screenConfig, WTypes::HostList hostConfig);
//    void backgroundColorChanged(QColor color);
//    void pathsChanged(QStringList paths, bool recursive);
//    void changeScreenConfig(WTypes::ScreenList screenConfig, WTypes::HostList hostConfig);
    
public slots:
    void openSettings();
    void applySettings();
    void quit();
    void explicitlyChangeWallpaper();
    void deleteWallpaper();

    void wallpaperChanged(QVector<WTypes::Wallpaper> wallpapers);
    void hostConfigChanged(WTypes::ScreenList screens, WTypes::HostList hosts);
//    void serverListening(quint16 port);
    
    void readSettingsAndApply(quint16 explicitClientPort = 0);
private:
    void writeScreenConfig(WTypes::ScreenList screens, WTypes::HostList hosts, bool writeTweaked);
    void getAndWriteSettings();

    QApplication *app;
    QThread serverThread, clientThread;
    ImageRepository imageRepository;
    NetworkClient client;
    WallpaperServer server;
    WallpaperChanger wpchanger;
    ConfigDialog dialog;

    QTimer timer;
    QStringList currentHostConfig;
};

#endif // WALLE_H
